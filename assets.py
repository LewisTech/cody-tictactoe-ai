gridLine = "████████████████████████\n"

gridLineUpperXXX = "██ \  / █ \  / █ \  / ██\n"
gridLineMiddleXXX = "██  ><  █  ><  █  ><  ██\n"
gridLineLowerXXX = "██ /  \ █ /  \ █ /  \ ██\n"
gridLineXXX = gridLineUpperXXX + gridLineMiddleXXX + gridLineLowerXXX

gridLineUpperXXO = "██ \  / █ \  / █ ┌──┐ ██\n"
gridLineMiddleXXO = "██  ><  █  ><  █ │  │ ██\n"
gridLineLowerXXO = "██ /  \ █ /  \ █ └──┘ ██\n"
gridLineXXO = gridLineUpperXXO + gridLineMiddleXXO + gridLineLowerXXO

gridLineUpperXOX = "██ \  / █ ┌──┐ █ \  / ██\n"
gridLineMiddleXOX = "██  ><  █ │  │ █  ><  ██\n"
gridLineLowerXOX = "██ /  \ █ └──┘ █ /  \ ██\n"
gridLineXOX = gridLineUpperXOX + gridLineMiddleXOX + gridLineLowerXOX

gridLineUpperXOO = "██ \  / █ ┌──┐ █ ┌──┐ ██\n"
gridLineMiddleXOO = "██  ><  █ │  │ █ │  │ ██\n"
gridLineLowerXOO = "██ /  \ █ └──┘ █ └──┘ ██\n"
gridLineXOO = gridLineUpperXOO + gridLineMiddleXOO + gridLineLowerXOO

gridLineUpperXNN = "██ \  / █      █      ██\n"
gridLineMiddleXNN = "██  ><  █      █      ██\n"
gridLineLowerXNN = "██ /  \ █      █      ██\n"
gridLineXNN = gridLineUpperXNN + gridLineMiddleXNN + gridLineLowerXNN

gridLineUpperXXN = "██ \  / █ \  / █      ██\n"
gridLineMiddleXXN = "██  ><  █  ><  █      ██\n"
gridLineLowerXXN = "██ /  \ █ /  \ █      ██\n"
gridLineXXN = gridLineUpperXXN + gridLineMiddleXXN + gridLineLowerXXN

gridLineUpperXON = "██ \  / █ ┌──┐ █      ██\n"
gridLineMiddleXON = "██  ><  █ │  │ █      ██\n"
gridLineLowerXON = "██ /  \ █ └──┘ █      ██\n"
gridLineXON = gridLineUpperXON + gridLineMiddleXON + gridLineLowerXON

gridLineUpperXNO = "██ \  / █      █ ┌──┐ ██\n"
gridLineMiddleXNO = "██  ><  █      █ │  │ ██\n"
gridLineLowerXNO = "██ /  \ █      █ └──┘ ██\n"
gridLineXNO = gridLineUpperXNO + gridLineMiddleXNO + gridLineLowerXNO

gridLineUpperXNX = "██ \  / █      █ ┌──┐ ██\n"
gridLineMiddleXNX = "██  ><  █      █ │  │ ██\n"
gridLineLowerXNX = "██ /  \ █      █ └──┘ ██\n"
gridLineXNX = gridLineUpperXNX + gridLineMiddleXNX + gridLineLowerXNX


gridLineUpperOXX = "██ ┌──┐ █ \  / █ \  / ██\n"
gridLineMiddleOXX = "██ │  │ █  ><  █  ><  ██\n"
gridLineLowerOXX = "██ └──┘ █ /  \ █ /  \ ██\n"
gridLineOXX = gridLineUpperOXX + gridLineMiddleOXX + gridLineLowerOXX

gridLineUpperOXO = "██ ┌──┐ █ \  / █ ┌──┐ ██\n"
gridLineMiddleOXO = "██ │  │ █  ><  █ │  │ ██\n"
gridLineLowerOXO = "██ └──┘ █ /  \ █ └──┘ ██\n"
gridLineOXO = gridLineUpperOXO + gridLineMiddleOXO + gridLineLowerOXO

gridLineUpperOOX = "██ ┌──┐ █ ┌──┐ █ \  / ██\n"
gridLineMiddleOOX = "██ │  │ █ │  │ █  ><  ██\n"
gridLineLowerOOX = "██ └──┘ █ └──┘ █ /  \ ██\n"
gridLineOOX = gridLineUpperOOX + gridLineMiddleOOX + gridLineLowerOOX

gridLineUpperOOO = "██ ┌──┐ █ ┌──┐ █ ┌──┐ ██\n"
gridLineMiddleOOO = "██ │  │ █ │  │ █ │  │ ██\n"
gridLineLowerOOO = "██ └──┘ █ └──┘ █ └──┘ ██\n"
gridLineOOO = gridLineUpperOOO + gridLineMiddleOOO + gridLineLowerOOO

gridLineUpperONN = "██ ┌──┐ █      █      ██\n"
gridLineMiddleONN = "██ │  │ █      █      ██\n"
gridLineLowerONN = "██ └──┘ █      █      ██\n"
gridLineONN = gridLineUpperONN + gridLineMiddleONN + gridLineLowerONN

gridLineUpperOXN = "██ ┌──┐ █ \  / █      ██\n"
gridLineMiddleOXN = "██ │  │ █  ><  █      ██\n"
gridLineLowerOXN = "██ └──┘ █ /  \ █      ██\n"
gridLineOXN = gridLineUpperOXN + gridLineMiddleOXN + gridLineLowerOXN

gridLineUpperOON = "██ ┌──┐ █ ┌──┐ █      ██\n"
gridLineMiddleOON = "██ │  │ █ │  │ █      ██\n"
gridLineLowerOON = "██ └──┘ █ └──┘ █      ██\n"
gridLineOON = gridLineUpperOON + gridLineMiddleOON + gridLineLowerOON

gridLineUpperONO = "██ ┌──┐ █      █ ┌──┐ ██\n"
gridLineMiddleONO = "██ │  │ █      █ │  │ ██\n"
gridLineLowerONO = "██ └──┘ █      █ └──┘ ██\n"
gridLineONO = gridLineUpperONO + gridLineMiddleONO + gridLineLowerONO

gridLineUpperONX = "██ ┌──┐ █      █ \  / ██\n"
gridLineMiddleONX = "██ │  │ █      █  ><  ██\n"
gridLineLowerONX = "██ └──┘ █      █ /  \ ██\n"
gridLineONX = gridLineUpperONX + gridLineMiddleONX + gridLineLowerONX


gridLineUpperNXX = "██      █ \  / █ \  / ██\n"
gridLineMiddleNXX = "██      █  ><  █  ><  ██\n"
gridLineLowerNXX = "██      █ /  \ █ /  \ ██\n"
gridLineNXX = gridLineUpperNXX + gridLineMiddleNXX + gridLineLowerNXX

gridLineUpperNXO = "██      █ \  / █ ┌──┐ ██\n"
gridLineMiddleNXO = "██      █  ><  █ │  │ ██\n"
gridLineLowerNXO = "██      █ /  \ █ └──┘ ██\n"
gridLineNXO = gridLineUpperNXO + gridLineMiddleNXO + gridLineLowerNXO

gridLineUpperNOX = "██      █ ┌──┐ █ \  / ██\n"
gridLineMiddleNOX = "██      █ │  │ █  ><  ██\n"
gridLineLowerNOX = "██      █ └──┘ █ /  \ ██\n"
gridLineNOX = gridLineUpperNOX + gridLineMiddleNOX + gridLineLowerNOX

gridLineUpperNOO = "██      █ ┌──┐ █ ┌──┐ ██\n"
gridLineMiddleNOO = "██      █ │  │ █ │  │ ██\n"
gridLineLowerNOO = "██      █ └──┘ █ └──┘ ██\n"
gridLineNOO = gridLineUpperNOO + gridLineMiddleNOO + gridLineLowerNOO

gridLineUpperNNN = "██      █      █      ██\n"
gridLineMiddleNNN = "██      █      █      ██\n"
gridLineLowerNNN = "██      █      █      ██\n"
gridLineNNN = gridLineUpperNNN + gridLineMiddleNNN + gridLineLowerNNN

gridLineUpperNXN = "██      █ \  / █      ██\n"
gridLineMiddleNXN = "██      █  ><  █      ██\n"
gridLineLowerNXN = "██      █ /  \ █      ██\n"
gridLineNXN = gridLineUpperNXN + gridLineMiddleNXN + gridLineLowerNXN

gridLineUpperNON = "██      █ ┌──┐ █      ██\n"
gridLineMiddleNON = "██      █ │  │ █      ██\n"
gridLineLowerNON = "██      █ └──┘ █      ██\n"
gridLineNON = gridLineUpperNON + gridLineMiddleNON + gridLineLowerNON

gridLineUpperNNO = "██      █      █ ┌──┐ ██\n"
gridLineMiddleNNO = "██      █      █ │  │ ██\n"
gridLineLowerNNO = "██      █      █ └──┘ ██\n"
gridLineNNO = gridLineUpperNNO + gridLineMiddleNNO + gridLineLowerNNO

gridLineUpperNNX = "██      █      █ \  / ██\n"
gridLineMiddleNNX = "██      █      █  ><  ██\n"
gridLineLowerNNX = "██      █      █ /  \ ██\n"
gridLineNNX = gridLineUpperNNX + gridLineMiddleNNX + gridLineLowerNNX

defaultGrid = gridLine + gridLineNNN + gridLine + gridLineNNN + gridLine + gridLineNNN + gridLine

