import time
import sys
import os
gameState = {"A1":"N","A2":"N","A3":"N","B1":"N","B2":"N","B3":"N","C1":"N","C2":"N","C3":"N"}
f = open("currentGame.py", "w").close()
os.remove("currentGame.py")
f = open("currentGame.py", "w")
f.write("AISymbol = \"N\"\n")
f.write("userSymbol = \"N\"\n")
f.write("gameState = " + str(gameState) + "\n")
f.close()
import logic
from logic import (endGame, chooseMove)
from assets import (gridLineXXX, gridLineXXO, gridLineXOX, gridLineXOO,
gridLineXNN, gridLineXXN, gridLineXON, gridLineXNO,
gridLineXNX, gridLineOXX, gridLineOXO, gridLineOOX,
gridLineOOO, gridLineONN, gridLineOXN, gridLineOON,
gridLineONO, gridLineONX, gridLineNXX, gridLineNXO,
gridLineNOX, gridLineNOO, gridLineNNN, gridLineNXN,
gridLineNON, gridLineNNO, gridLineNNX, gridLine, defaultGrid)
acceptableMoves = ["A1", "A2", "A3", "B1", "B2", "B3", "C1", "C2", "C3"]
remainingMoves = ["A1", "A2", "A3", "B1", "B2", "B3", "C1", "C2", "C3"]

def main():
    print("Welcome to the Tic Tac Toe, or Noughts and Crosses, game.")
#    time.sleep(5)
    print("Users can play against the computer using commands.")
#    time.sleep(5)
    print("Users will need to enter grid references to add symbols.")
#    time.sleep(5)
    print("A1, A2, A3, B1, B2, B3, C1, C2, and C3 are valid grid references.")
#    time.sleep(3)
    global userSymbol
    global AISymbol
    userSymbol = input("\nWould you like to play as Noughts (Enter O) or Crosses (Enter X)? ").upper()
    while userSymbol != "X" and userSymbol != "O":
        userSymbol = input("Would you like to play as Noughts (Enter O) or Crosses (Enter X)? ").upper()
    if userSymbol == "X":
        AISymbol = "O"
    elif userSymbol == "O":
        AISymbol = "X"
    f = open("currentGame.py", "w")
    f.write("AISymbol = \"" + AISymbol + "\"\n")
    f.write("userSymbol = \"" + userSymbol + "\"\n")
    f.close()
    print(remainingMoves)
    for l in "Initialising...":
        sys.stdout.write(l)
        sys.stdout.flush()
        time.sleep(0.1)
    print('\n' + defaultGrid)
    time.sleep(0.5)
    userTurn()
    

def render():
    A1 = gameState["A1"]
    A2 = gameState["A2"]
    A3 = gameState["A3"]
    B1 = gameState["B1"]
    B2 = gameState["B2"]
    B3 = gameState["B3"]
    C1 = gameState["C1"]
    C2 = gameState["C2"]
    C3 = gameState["C3"]
    
    print(gridLine + eval("gridLine" + A1 + A2 + A3)
          + gridLine + eval("gridLine" + B1 + B2 + B3)
          + gridLine + eval("gridLine" + C1 + C2 + C3) + gridLine)

def userTurn():
    f = open("currentGame.py", "w")
    f.write(gameState) 
    f.close()
    userMove = input("Your move.\n").upper()
    while userMove not in acceptableMoves and userMove in remainingMoves:
        userMove  = input("Please enter an acceptable move (A-C, 1-3). ").upper()
    moveOkay = False
    try:
        remainingMoves.remove(userMove)
        moveOkay = True
    except ValueError:
        pass
    if moveOkay != True:
        userMove = input("Your move.\n").upper()
        while userMove not in acceptableMoves and userMove in remainingMoves:
            userMove  = input("Please enter an acceptable move (A-C, 1-3). ").upper()
    else:
        pass
    print(remainingMoves)
    currentGame[userMove] = userSymbol
    render()
    endGame()
    if endGame() == True:
        gameOver()
    else:
        AITurn()
       
def AITurn():
    f = open("currentGame.py", "w")
    f.write(gameState)
    f.close()
    logic.chooseMove()
    from logic import AIMove
    currentGame[AIMove] = AISymbol
    remainingMoves.remove(AIMove)
    print(remainingMoves)
    print("Computer's Move")
    render()
    endGame()
    if endGame() == True:
        gameOver()
    else:
        userTurn()

def gameOver():
    print("Game Over! The winner was " + currentGame.winner + ".\n")
    exit(0)

main()

